from django.contrib import admin

from .forms import LabelForm
from .models import Issue, Label, Milestone, Sprint, Tag, Task


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('__str__',)
    search_fields = ('tag',)


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'color')
    search_fields = ('label',)
    form = LabelForm


@admin.register(Milestone)
class MilestoneAdmin(admin.ModelAdmin):
    list_display = ('__str__',)
    search_fields = ('title',)


class IssueInline(admin.TabularInline):
    model = Issue
    extra = 0


@admin.register(Sprint)
class SprintAdmin(admin.ModelAdmin):
    inlines = (IssueInline,)
    list_display = ('__str__', 'project')
    list_filter = ('project',)


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'get_labels', 'milestone', 'sprint', 'status')
    readonly_fields = ('slug',)
    search_fields = ('title',)
    list_filter = ('status', 'labels', 'milestone')
    date_hierarchy = 'created'

    @admin.display(description='labels')
    def get_labels(self, obj):
        return ','.join(obj.labels.values_list('label', flat=True))


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'project', 'issue', 'status', 'start_time', 'end_time')
    readonly_fields = ('slug',)
    search_fields = ('title',)
    list_filter = ('status', 'tags')
    date_hierarchy = 'created'
